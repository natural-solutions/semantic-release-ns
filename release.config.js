module.exports = {
  plugins: [
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",
    "@semantic-release/changelog",
    [
      "@semantic-release/exec",
      {
        prepareCmd: "npm version --no-git-tag-version ${nextRelease.version}",
      },
    ],
    [
      "@semantic-release/git",
      {
        assets: ["package*.json", "CHANGELOG.md"],
        message: "chore(release): ${nextRelease.version} [skip ci]",
      },
    ],
    "@semantic-release/gitlab",
  ],
  branches: ["main", "master"],
};
