FROM node:slim
ENV NODE_ENV=production
RUN apt-get -y update \ 
  && apt-get -y install git \
  && apt-get -y clean
WORKDIR /app
RUN npm install -g semantic-release \
 @semantic-release/changelog \
 @semantic-release/commit-analyzer \
 @semantic-release/exec \
 @semantic-release/git \ 
 @semantic-release/gitlab \ 
 @semantic-release/release-notes-generator \
 @semantic-release/npm
COPY release.config.js ./
