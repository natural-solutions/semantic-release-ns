# Semantic Release Image for NS

## Fonctionnement

- https://gitlab.com/natural-solutions/semantic-release-ns.git

On utilise le CI/CD pour gerer le semantic versionning.

Image docker sur server NS01 : rien à installer sur chaque app a part les fichiers de configuration suivants

Quand on fait un push sur une branche : respecter un format de commit (CF: commitlint.config.js).
A chaque commit ca va sur le stage release qui récupere l'image docker, execute un script et TADAM. Le script vérifie que le commit est bien écrit et il implemente la version.

## URL image docker

```bash
registry.gitlab.com/natural-solutions/semantic-release-ns:latest
```

# Sur votre projet:

## Ajouter les fichiers de configuration à la racine :

### Config CI/CD: .gitlab-ci.yml

```yml
stages:
  - release

release:
  stage: release
  image: registry.gitlab.com/natural-solutions/semantic-release-ns:latest
  script:
    - cp /app/release.config.js .
    - semantic-release
  tags:
    - NSCICDDOCKER
  only:
    - main #NOM DE LA BRANCHE SUR LAQUELLE FAIRE LE SEMANTIC VERSIONNING
```

### Config commitlint : commitlint.config.js

- https://github.com/conventional-changelog/commitlint

Gère la configuration du commit :

- Commencer par le type de commit `type-enum` (Ex. : `commit -m "fix: my fix message"`)

```JSON
module.exports = {

  parserPreset: "conventional-changelog-conventionalcommits",
  rules: {
      "body-leading-blank": [1, "always"],
      "body-max-line-length": [2, "always", 100],
      "footer-leading-blank": [1, "always"],
      "footer-max-line-length": [2, "always", 100],
      "header-max-length": [2, "always", 100],
      "scope-case": [2, "always", "lower-case"],
      "subject-case": [2, "never", ["sentence-case", "start-case", "pascal-case", "upper-case"], ],
      "subject-empty": [2, "never"],
      "subject-full-stop": [2, "never", "."],
      "type-case": [2, "always", "lower-case"],
      "type-empty": [2, "never"],
      "type-enum": [2, "always", ["build", "chore", "ci", "docs", "feat", "fix", "perf", "refactor", "revert", "style", "test", ], ],
  },
};

```

### Config version : .releaserc

Sert a implémenter la gestion des versions :

```JSON
{
  "plugins": [
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",
    ["@semantic-release/exec", {
      "verifyReleaseCmd": "echo ${nextRelease.version} > VERSION"
    }],
    "@semantic-release/changelog",
    ["@semantic-release/git", {
      "assets": ["VERSION", "CHANGELOG.md"],
      "message": "chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}"
    }],
    "@semantic-release/gitlab"
  ],
  "branches": ["main"],    #NOM DE LA BRANCHE SUR LAQUELLE FAIRE LE SEMANTIC VERSIONNING
}
```

### Ajouter dossier : .husky

Gère les hook gitlab (Copier coller à partir d'un projet existant. Ex : https://gitlab.com/natural-solutions/reneco-ecollection-veto-front/-/tree/master-dev/.husky)

## Lancer les commandes dans votre projet

### WINDOWS

```bash
 npm install --save-dev @commitlint/config-conventional @commitlint/cli
```

```bash
npm install husky --save-dev
```

```bash
npx husky install
```

### MAC / LINUX

TODO

## Dans le gitLab du projet

Aller dans settings > CI/CD > variables > ajouter variables :

- GL_TOKEN : ne pas mettre en protected
